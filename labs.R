---
  title: "REVIEW"
author: "Hovig Artinian"
date: "November 10, 2018"
output: html_document
---

Outliers:
  
To detect outliers:
boxplot
scatterplot
histogram
boxplot(x, horizontal = TRUE) # x is the data set

boxplot.stats(x)$out

HISTOGRAM:
To see the distribution of the data
try to normalize the shape of the curve



kolkhara=boxplot.stats(Auto$horsepower)$out

kay=Auto$horsepower[!Auto$horsepower %in% boxplot.stats(Auto$horsepower)$out]

Step 1, get the  Interquartile Range
Step 2, calculate the upper and lower values
Step 3, remove anything greater than max, or less than min.


#Save the data in a variable
data.iqr= exam.data

# Create a variable of the column names you want to remove outliers on.
vars=c("age")

# Create a variable to store the row id's to be removed
Outliers=c()

# Loop through the list of columns you specified
for(i in vars){
  
  # Get the Min/Max values
  max=quantile(data.iqr[,i],0.75, na.rm=TRUE) + (IQR(data.iqr[,i], na.rm=TRUE) * 1.5 )
  min=quantile(data.iqr[,i],0.25, na.rm=TRUE) - (IQR(data.iqr[,i], na.rm=TRUE) * 1.5 )
    
  # Get the id's using which
  idx=which(data.iqr[,i] < min | data.iqr[,i] > max)
    
  # Output the number of outliers in each variable
  print(paste(i, length(idx), sep=''))
    
  # Append the outliers list
  Outliers=c(Outliers, idx) 
}

# Sort
Outliers=sort(Outliers)
Outliers
# Remove the outliers
data.iqr=data.iqr[-Outliers,]



#CROSS VALIDATION

val.errors=rep(NA,13)
for(i in 1:13){
  coefi=coef(regfit.best,id=i)
  pred=test.mat[,names(coefi)]%*%coefi
  val.errors[i]=mean((exam.data01$sex[test]-pred)^2)
}
val.errors

test.mat=model.matrix(sex~., data=exam.data01[test,])

regfit.best=regsubsets(sex~., data=exam.data01[train,],nvmax=13)


set.seed(1)
cv.error=rep(NA, 13)
for(i in 1:13){
  glm.fit=glm(sex~., data=exam.data01, family="binomial")
  cv.error[i]=cv.glm(exam.data01, glm.fit, K=10)$delta[1]
}
cv.error


First, we create a vector that allocates each observation to one of k = 10 folds, and we create a matrix in which we will store the results.

```{r}
k=10
set.seed(1)
folds=sample(1:k, nrow(Hitters), replace=TRUE)
cv.errors=matrix(NA, k, 19, dimnames=list(NULL, paste(1:19)))
```

Now, we write a for loop that performs cross-validation:
  
1) In the *j*th fold, the elements of `folds` that equal `j` are in the test set, and the remainder are in the training set. 
2) We make our predictions for each model size
3) Compute the test errors on the appropriate subset
4) Store them in the appropriate slot in the matrix `cv.errors`

```{r}
for(j in 1:k){
  best.fit=regsubsets(Salary~., data=Hitters[folds!=j,],nvmax=19)
  for(i in 1:19){
    pred=predict.regsubsets(best.fit, Hitters[folds==j,],id=i)
    cv.errors[j,i]=mean((Hitters$Salary[folds==j]-pred)^2)
  }
}
```

This has given us a 10??19 matrix, of which the (*i*, *j*)th element corresponds to the test MSE for the *i*th cross-validation fold for the best *j*-variable model.

We now use the `apply()` function to average over the columns of this matrix in order to obtain a vector for which the *j*th element is the cross-validation error for the *j*-variable model.

```{r}
mean.cv.errors=apply(cv.errors, 2, mean)
mean.cv.errors
par(mfrow=c(1,1))
plot(mean.cv.errors, type='b')
```

We see that cross-validation selects an 11-variable model. 

```{r}
coef(best.fit, which.min(mean.cv.errors))
```

We now perform `Best Subset Selection` on the full data set in order to obtain the 11-variable model.

```{r}
reg.best=regsubsets(Salary~., data=Hitters, nvmax=19)
coef(reg.best, 11)
```
Again, we obtain different results.


**OUTLIERS**

which.outlier() #use it to detect outliers

#to get the predictors in pls
which(pls.fit$loadings[,2]>0.1 |pls.fit$loadings[,2]<(-0.1))
names(which(pls.fit$loadings[,2]>0.1 |pls.fit$loadings[,2]<(-0.1)))

#to see which component to take
which.min(MSEP(pcr.fit)$val[1,1,])